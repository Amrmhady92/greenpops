﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleDown : MonoBehaviour
{
    bool moving = false;
    public float range = 10;
    private void OnEnable()
    {
        float directionX = Random.Range(-1f, 1f);
        float directionY = Random.Range(-1f, 1f);
        transform.localScale = Vector3.one;
        transform.LeanMove((new Vector3(directionX, directionY, this.transform.position.z) * range) + this.transform.position, 1);
        transform.LeanScale(Vector3.zero, 0.6f).setOnComplete(()=> 
        {
            LeanTween.cancel(gameObject);
            this.gameObject.SetActive(false);
        });
    }
    

}
