﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseCursor : MonoBehaviour
{

    public SpriteRenderer mouseSprite;

    // Start is called before the first frame update
    void Start()
    {
        Cursor.visible = false;
    }
    Vector3 cursorPos;
    // Update is called once per frame
    void FixedUpdate()
    {
        cursorPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        //cursorPos.z = 0;
        transform.position = cursorPos;
    }
}
