﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CirclesSpawner : MonoBehaviour
{
    [Header("Time Between Circles")]
    public float spawnRateMin = 3f;
    public float spawnRateMax = 5f;

    private float spawnRate = 4f;
    public GreenCircle circlePrefab;
    private Pooler pooler;


    public float maxXScreen = 9f, minXScreen = -9f, maxYScreen = 5f, minYScreen = -5f;
    float maxX , minX , maxY, minY;
    private int currentQuarter = 0;
    private List<int> screenQuarters;
    void Start()
    {
        screenQuarters = new List<int>() { 0, 1, 2, 3 };
        screenQuarters.Shuffle();


        pooler = this.gameObject.AddComponent<Pooler>();
        pooler.PooledObject = circlePrefab.gameObject;
    }

    public void StartSpawing()
    {
        spawnRate = Random.Range(spawnRateMin, spawnRateMax);
        StartCoroutine(StartSpawning());
    }

    public void StopSpawing(bool shrinkAllCircles = true)
    {
        GreenCircle circle;
        StopAllCoroutines();
        if (shrinkAllCircles)
        {
            for (int i = 0; i < pooler.pool.Count; i++)
            {
                circle = pooler.pool[i].GetComponent<GreenCircle>();
                if (circle != null) circle.Shrink(0.1f);
            }
        }
        
    }


    public IEnumerator StartSpawning()
    {
        

        if (screenQuarters.Count > 0)
        {
            currentQuarter = screenQuarters[0];
            screenQuarters.RemoveAt(0);
        }

        if(screenQuarters.Count == 0)
        {
            screenQuarters = new List<int>() { 0, 1, 2, 3 };
            screenQuarters.Shuffle();
        }

        // Screen Quarters
        // 0  1
        // 2  3
        switch (currentQuarter)
        {
            case 0:
                minX = minXScreen;
                maxX = 0;

                minY = 0;
                maxY = maxYScreen;
                break;

            case 1:
                minX = 0;
                maxX = maxXScreen;

                minY = 0;
                maxY = maxYScreen;
                break;

            case 2:
                minX = minXScreen;
                maxX = 0;

                minY = minYScreen;
                maxY = 0;
                break;

            case 3:
                minX = 0;
                maxX = maxXScreen;

                minY = minYScreen;
                maxY = 0;
                break;
        }

        GreenCircle circle = pooler.Get(true).GetComponent<GreenCircle>();
        float x, y = 0;

        if (minX < maxX) x = Random.Range(minX, maxX);
        else x = Random.Range(maxX, minX);

        if (minY < maxY) y = Random.Range(minY, maxY);
        else y = Random.Range(maxY, minY);

        Vector3 targetPosition = new Vector3(x, y, 0);
        //Vector3 targetPosition = Camera.main.ScreenToWorldPoint(new Vector3(x, y, 0));
        //targetPosition.z = 10;
        circle.transform.position = targetPosition;// new Vector3(x, y, circle.transform.position.z);
        circle.Grow();


        spawnRate = Random.Range(spawnRateMin, spawnRateMax);
        yield return new WaitForSeconds(spawnRate);
        StartCoroutine(StartSpawning());


    }
}
