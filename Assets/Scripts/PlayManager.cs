﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayManager : MonoBehaviour
{
    public int maxScoreForBlackEnding = 10;

    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI endingText;
    [Space(10)]
    public RectTransform endingGreenImage;
    public RectTransform endingBlackImage;
    public RectTransform endingBlackWallsImage;
    public GameObject restartButton;
    [Space(10)]
    public AudioClip gunshotSound;
    public AudioClip greenEndSound;
    public AudioClip blackEndingMusic;
    public AudioClip greenEndingMusic;

    private AudioSource aSource;
    [SerializeField] private AudioSource musicASource;
    [Space(10)]
    public CirclesSpawner spawner;
    public int scorePerClick = 1;

    public float spawnerSlowingRate = 0.1f;
    public int spawnerCountToSlow = 5;

    public float maxSlowRateMin = 1.5f;
    public float maxSlowRateMax = 2.5f;

    public Texture2D[] mouseTextures;

    private int score = 0;
    public bool calculateBlackening = false;
    [SerializeField] private float blackingRate = 0.01f;
    public static System.Action onScoreChanged; 
    public static System.Action onGreenPrevail;
    //public static bool ended = false;
    public SpriteRenderer bgSprite;
    public Color bgColorOnGreen = Color.cyan;
    public static bool ended = false;
    int mouseChangingCount = 1;
    int mouseCountIndex = 0;
    public int Score
    {
        get
        {
            return score;
        }

        set
        {
            if(score != value)
            {
                //scoreText.text = "Circles : " + value.ToString();
                //scoreText.text += "/";
                //if(value%5 == 0)
                //{
                //    scoreText.text += " ";
                //}
            }
            score = value;
        }
    }
    
    private void Start()
    {

        Debug.Log(onScoreChanged);
        onScoreChanged = new System.Action(OnScoreModified);
        onGreenPrevail = new System.Action(GreenPrevailed);

        Debug.Log(onScoreChanged);
        if (calculateBlackening)
        {
            blackingRate = (float)scorePerClick / (float)maxScoreForBlackEnding;
        }
        aSource = this.gameObject.AddComponent<AudioSource>();
        if(musicASource == null) musicASource = this.gameObject.AddComponent<AudioSource>();
        aSource.playOnAwake = false;
        musicASource.playOnAwake = false;

        Debug.Log(blackingRate);

        if(mouseTextures.Length > 0)
        {
            mouseChangingCount = maxScoreForBlackEnding / mouseTextures.Length;
            Cursor.SetCursor(mouseTextures[0],Vector2.zero,CursorMode.Auto);
            mouseCountIndex = 0;
        }
        musicASource.loop = true;
        musicASource.clip = blackEndingMusic;
        musicASource.Play();

        ended = false;
       
    }

    public void StartGame()
    {
        spawner.StartSpawing();
        endingText.gameObject.SetActive(false);
    }

    public void OnScoreModified()
    {
        Score += scorePerClick;
        Blacken();
        if(Score  >= maxScoreForBlackEnding)
        {
            //Game Ended Stop
  
            BlackPrevailed();
        }
        if((Score / scorePerClick)%spawnerCountToSlow == 0)
        {
            //spawner.spawnRateMax += spawnerSlowingRate;
            //spawner.spawnRateMin += spawnerSlowingRate;
            spawner.spawnRateMax -= spawnerSlowingRate;
            spawner.spawnRateMin -= spawnerSlowingRate;

            spawner.spawnRateMax = Mathf.Max(spawner.spawnRateMax, maxSlowRateMax);
            spawner.spawnRateMin = Mathf.Max(spawner.spawnRateMin, maxSlowRateMin);


        }

        if(Score%mouseChangingCount == 0 && mouseTextures.Length > 0)
        {
            mouseCountIndex++;
            if(mouseTextures.Length > mouseCountIndex)
            {
                Cursor.SetCursor(mouseTextures[mouseCountIndex], Vector2.zero, CursorMode.Auto);
            }
        }

    }

    public void GreenPrevailed()
    {
        if (ended) return;
        ended = true;
        spawner.StopSpawing(false);
        endingText.gameObject.SetActive(true);
        endingText.color = Color.white;
        endingText.text = "The People now have their freedom";
        endingText.transform.localScale = Vector3.zero;
        endingText.transform.LeanScale(Vector3.one, 1);
        endingGreenImage.LeanAlpha(1,0.5f);
        //aSource.clip = gunshotSound;
        //aSource.Play();
        restartButton.SetActive(true);
        musicASource.clip = greenEndingMusic;
        musicASource.Play();

    }

    public void BlackPrevailed()
    {
        if (ended) return;
        ended = true;
        spawner.StopSpawing();
        endingText.gameObject.SetActive(true);
        endingText.color = Color.white;
        endingText.text = "The People are now under Control";
        endingText.transform.localScale = Vector3.zero;
        endingText.transform.LeanScale(Vector3.one, 1);
        endingBlackImage.LeanAlpha(1, 0.5f);
        endingBlackWallsImage.LeanMoveLocalY(0, 1);

        aSource.clip = gunshotSound;
        aSource.Play();
        musicASource.Stop();

        restartButton.SetActive(true);

    }

    public void Blacken()
    {

        if (bgSprite.color.r > 0.05f)
        {
            bgSprite.color = new Color(bgSprite.color.r - blackingRate, bgSprite.color.b - blackingRate, bgSprite.color.g - blackingRate);
        }
        else
        {
            bgSprite.color = Color.black;
        }

        
    }

    public void RestartGame()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }
}
