﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GreenCircle : MonoBehaviour
{
    public float timeForFullCircleMax = 20;
    public float timeForFullCircleMin = 40;
    private List<GameObject> circleEffects;
    public GameObject circleEffectPrefab;
    public int circleEffectCount = 7;
    private float timeForFullCircle;

    public SpriteRenderer rend;
    public Sprite[] spritesShapes;


    public float maxScale = 240;
    public bool active = true;
    private bool started = false;
    private void Awake()
    {
        if (started) return;
        if (circleEffectPrefab != null)
        {
            circleEffects = new List<GameObject>();
            for (int i = 0; i < circleEffectCount; i++)
            {
                circleEffects.Add(Instantiate(circleEffectPrefab));
               // circleEffects[i].SetActive(false);
            }
        }


        started = true;
    }
    public void Grow()
    {
        Sprite currentSprite;
        if(spritesShapes.Length > 0)
        {
            currentSprite= spritesShapes.GetRandomValue();
            rend.sprite = currentSprite;
            SpriteRenderer effectRend;
            for (int i = 0; i < circleEffects.Count; i++)
            {
                effectRend = circleEffects[i].GetComponent<SpriteRenderer>();
                if (effectRend != null) effectRend.sprite = currentSprite;
            }
        }
        this.transform.eulerAngles = new Vector3(0, 0, Random.Range(0, 360f));

        active = true; 
        this.transform.localScale = Vector3.zero;
        //int maxScale = Screen.width >= Screen.height ? Screen.width : Screen.height;
        Vector3 targetScale = new Vector3(maxScale, maxScale, 1);
        timeForFullCircle = Random.Range(timeForFullCircleMin, timeForFullCircleMax);
        this.transform.LeanScale(targetScale, timeForFullCircle)/*.setEaseOutCubic()*/.setOnComplete(OnGrowComplete);
    }

    //private void Update()
    //{
        
    //}

    private void OnMouseDown()
    {
        if (PlayManager.ended) return;
        if (!active) return;
        active = false;
        Shrink();
        PlayManager.onScoreChanged();

        for (int i = 0; i < circleEffects.Count; i++)
        {
            circleEffects[i].transform.position = this.transform.position;
            circleEffects[i].SetActive(true);
        }
    }

    public void Shrink(float shrinkSpeed = 0.3f)
    {
        LeanTween.cancel(this.gameObject);
        this.transform.LeanScale(Vector3.zero, shrinkSpeed).setOnComplete(OnShrinked);
    }

    private void OnGrowComplete()
    {
        Debug.Log("Game over Green");
        PlayManager.onGreenPrevail.Invoke();
    }

    private void OnShrinked()
    {
        this.gameObject.SetActive(false);
    }
}
